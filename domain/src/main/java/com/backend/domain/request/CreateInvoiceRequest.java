package com.backend.domain.request;

import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.List;
import java.util.UUID;

@Data
public class CreateInvoiceRequest {

    String customerId;
    String note;
    List<ItemRequest> itemRequestList;

}

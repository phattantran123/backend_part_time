package com.backend.domain.request;

import lombok.Data;

@Data
public class GetInvoiceRequest extends IndexPagingRequest {

    boolean date;
    private long fromDate;
    private long toDate;

    boolean money;
    private long fromMoney;
    private long toMoney;

    boolean numberItems;
    private long fromNumberItems;
    private long toNumberItems;


    private boolean sortByTotal;
    private boolean sortByDate;
    private boolean sortByNumberItems;


}

package com.backend.domain.request;

import lombok.Data;

@Data
public class ItemRequest {
    private String name;
    private long price;
    private String note;
}

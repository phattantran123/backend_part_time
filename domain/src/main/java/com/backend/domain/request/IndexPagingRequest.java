package com.backend.domain.request;

import lombok.Data;

@Data
public class IndexPagingRequest {

    private int page;
    private int limit;

    public int getLimit() {
        if (limit <= 0 || limit > 50)
            return 50;
        return limit;
    }
}

package com.backend.domain.config;

import com.beowulf.logging.model.LogMessageBuilder;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.ReadPreference;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.concurrent.TimeUnit;

@Slf4j
@Configuration
@EnableMongoRepositories({"com.backend.domain.*"})
public class MongoConfig {


    private static MongoTemplate mongoTemplate;
    private static MongoMappingContext mongoMappingContext;

    @Value("${mongo.db.name}")
    private String mongoDbName;
    @Value("${mongo.db.hosts}")
    private String mongoHosts;
    @Value("${mongo.db.port}")
    private String mongoPort;

    @Value("${mongo.db.ssl}")
    private boolean mongoSSL;
    @Value("${mongo.db.connection.pool}")
    private int mongoConnectionPool;

    public MongoConfig() {
        log.info("Init MongoConfig: {}", LogMessageBuilder.getInstance()
                .withAction("Init MongoConfig")
                .build());
    }

    @Bean
    public MongoTemplate mongoTemplate(SimpleMongoClientDatabaseFactory simpleMongoClientDatabaseFactory, MongoMappingContext context) {
        log.info("Start Connecting MongoDB: {} {} {}", LogMessageBuilder.getInstance()
                .withAction("connect")
                .withKeyAndValue("host", mongoHosts)
                .withKeyAndValue("dbName", mongoDbName)
                .build());
        context.setAutoIndexCreation(true);
        MappingMongoConverter converter = new MappingMongoConverter(new DefaultDbRefResolver(simpleMongoClientDatabaseFactory), context);
        converter.setTypeMapper(new DefaultMongoTypeMapper(null));

        MongoTemplate mongoTemplate = new MongoTemplate(simpleMongoClientDatabaseFactory, converter);
        MongoConfig.mongoTemplate = mongoTemplate;
        MongoConfig.mongoMappingContext = context;
        log.info("Connected MongoDB: {} {} {} {}", LogMessageBuilder.getInstance()
                .withAction("connected")
                .withKeyAndValue("host", mongoHosts)
                .withKeyAndValue("dbName", mongoDbName)
                .withMessage("Connect successfully")
                .build());
        return mongoTemplate;
    }

    @Bean
    public SimpleMongoClientDatabaseFactory simpleMongoClientDatabaseFactory(MongoClient mongoClient) {
        return new SimpleMongoClientDatabaseFactory(mongoClient, mongoDbName);
    }

    @Bean
    public MongoClient mongoClient(MongoClientSettings mongoClientSettings) {
        return MongoClients.create(mongoClientSettings);
    }

    @Bean
    public MongoClientSettings mongoClientSettings() {
        String connectionStr = String.format("mongodb://%s:%s/%s",
                mongoHosts,
                mongoPort,
                mongoDbName);

        MongoClientSettings.Builder builder = MongoClientSettings.builder()
                .applyConnectionString(new ConnectionString(connectionStr))
                .applyToSslSettings(sslBuilder -> {
                    sslBuilder.enabled(mongoSSL)
                            .invalidHostNameAllowed(true);
                })
                .applyToConnectionPoolSettings(poolSetting -> {
                    poolSetting.maxConnectionIdleTime(20000, TimeUnit.MILLISECONDS)
                            .maxSize(mongoConnectionPool);
                })
                .applyToSocketSettings(socketSetting -> {
                    socketSetting.connectTimeout(1800000, TimeUnit.MILLISECONDS)
                            .readTimeout(5000, TimeUnit.MILLISECONDS);
                })
                .readPreference(ReadPreference.primaryPreferred());
        return builder.build();
    }
}

package com.backend.domain.document;


import lombok.Data;
import org.bson.types.ObjectId;

@Data
public class ObjectIdDocument {
    private ObjectId id;
    private long created_at = System.currentTimeMillis();
    private long updated_at = System.currentTimeMillis();

}

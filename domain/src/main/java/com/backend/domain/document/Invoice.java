package com.backend.domain.document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;


@Data
@Document(collection = "invoice")
@AllArgsConstructor
public class Invoice extends ObjectIdDocument  {

    private UUID invoiceId;
    private long date;

    private long total;
    private String customerId;
    private String note;

    public Invoice(){
        this.setId(new ObjectId());
        this.invoiceId = UUID.randomUUID();
        this.date=System.currentTimeMillis();
    }
}

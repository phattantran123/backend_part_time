package com.backend.domain.document;

import com.backend.domain.request.ItemRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Data
@Document(collection = "items")
@NoArgsConstructor
@AllArgsConstructor
public class Item extends ObjectIdDocument{
    @Indexed(unique = true)
    private String itemId;

    private UUID invoiceId;
    private String name;
    private long price;
    private String note;

    public Item(UUID invoiceId, long name){
        this.setId(new ObjectId());
        this.setItemId(UUID.randomUUID().toString());
        this.setInvoiceId(invoiceId);
        this.setName(name+"");
        this.setPrice((long)(Math.random() * (10000 - 1000)) + 1000);
    }
    public Item(ItemRequest itemRequest){
        this.setId(new ObjectId());
        this.setItemId(UUID.randomUUID().toString());
        this.setName(itemRequest.getName());
        this.setPrice(itemRequest.getPrice());
        this.setNote(itemRequest.getNote());
    }
}

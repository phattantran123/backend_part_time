package com.backend.domain.repository.inf;

import com.backend.domain.document.Invoice;
import com.backend.domain.document.Item;
import com.backend.domain.repository.extend.InvoiceRepositoryExtend;
import com.backend.domain.repository.extend.ItemRepositoryExtend;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ItemRepository extends ObjectIdMongoRepository<Item>, ItemRepositoryExtend {
}

package com.backend.domain.repository.inf;

import com.backend.domain.document.Invoice;
import com.backend.domain.repository.extend.InvoiceRepositoryExtend;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.bson.types.ObjectId;

public interface InvoiceRepository extends ObjectIdMongoRepository<Invoice>, InvoiceRepositoryExtend {
}
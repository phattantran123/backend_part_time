package com.backend.domain.repository.impl;

import com.backend.domain.agg.InvoiceAggResult;
import com.backend.domain.repository.extend.InvoiceRepositoryExtend;
import com.backend.domain.request.GetInvoiceRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.bson.Document;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class InvoiceRepositoryImpl implements InvoiceRepositoryExtend {

    @Autowired
    MongoTemplate mongoTemplate;


    @Override
    public List<InvoiceAggResult> getListInvoice(GetInvoiceRequest request) {
        Criteria criteria = new Criteria();
        LookupOperation tutorLookupOperation = Aggregation.lookup("items", "invoiceId", "invoiceId", "item");
        //UnwindOperation tutorUnwindOperation = Aggregation.unwind("$item");
        AddFieldsOperation addFieldsOperation = Aggregation.addFields().addFieldWithValue("totalItems", new Document("$size", "$item")).build();
        if (request.isDate()){
            criteria = criteria.and("date").gte(request.getFromDate()).lte(request.getToDate());

        }else if (request.isMoney()){
            criteria = criteria.and("total").gte(request.getFromMoney()).lte(request.getToMoney());

        }else if (request.isNumberItems()){
            criteria = criteria.and("totalItems").gte(request.getFromNumberItems()).lte(request.getToNumberItems());
        }
        MatchOperation matchOperation = Aggregation.match(criteria);

        SortOperation sortOperation = Aggregation.sort(Sort.by(Sort.Direction.ASC, "_id"));

        if(request.isSortByDate()){
            sortOperation = Aggregation.sort(Sort.by(Sort.Direction.DESC, "date"));
        }else if (request.isSortByNumberItems()){
            sortOperation = Aggregation.sort(Sort.by(Sort.Direction.DESC, "totalItems"));
        }else if(request.isSortByTotal()){
            sortOperation = Aggregation.sort(Sort.by(Sort.Direction.DESC, "total"));
        }
        SkipOperation skipOperation = Aggregation.skip((long) (request.getPage() - 1) * request.getLimit());

        LimitOperation limitOperation = Aggregation.limit(request.getLimit());

        List<InvoiceAggResult> results = mongoTemplate.aggregate(Aggregation.newAggregation(
                tutorLookupOperation,
                addFieldsOperation,
                matchOperation,
                sortOperation,
                skipOperation,
                limitOperation), "invoice", InvoiceAggResult.class).getMappedResults();
         return new ArrayList<>(results);
    }
}

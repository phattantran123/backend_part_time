package com.backend.domain.repository.extend;

import com.backend.domain.agg.InvoiceAggResult;
import com.backend.domain.request.GetInvoiceRequest;

import java.util.List;

public interface InvoiceRepositoryExtend {
    public List<InvoiceAggResult> getListInvoice(GetInvoiceRequest request);
}

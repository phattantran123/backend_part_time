package com.backend.domain.components;

import com.backend.domain.ultils.SCryptKdfUtil;
import com.beowulf.logging.model.LogMessageBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ScryptKdfPasswordEncoder implements PasswordEncoder {

    @Override
    public String encode(CharSequence rawPassword) {
        try {
            return SCryptKdfUtil.encode(rawPassword.toString());
        } catch (Exception e) {
            log.error("{} {}", LogMessageBuilder.getInstance()
                    .withAction("Encode Password")
                    .withMessage(e.getMessage())
                    .build());
        }
        return null;
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        try {
            return SCryptKdfUtil.verify(rawPassword.toString(), encodedPassword);
        } catch (Exception e) {
            log.error("{} {}", LogMessageBuilder.getInstance()
                    .withAction("Decode Password")
                    .withMessage(e.getMessage())
                    .build());
        }
        return false;
    }
}
package com.backend.domain.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(setterPrefix = "set")
public class GeneralSubmitResponse extends BaseResponse {
    private boolean success;

    public static GeneralSubmitResponse success() {
        return new GeneralSubmitResponse(true);
    }

    public static GeneralSubmitResponse failed() {
        return new GeneralSubmitResponse(false);
    }
}

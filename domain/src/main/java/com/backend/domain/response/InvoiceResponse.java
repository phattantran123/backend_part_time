package com.backend.domain.response;

import com.backend.domain.agg.InvoiceAggResult;
import com.backend.domain.document.Invoice;
import com.backend.domain.document.Item;
import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Data
public class InvoiceResponse extends BaseResponse {
    @Indexed(unique = true)
    private UUID invoiceId;

    private long total;
    private long date;
    private String customerId;
    private String note;

    //get list item
    List<ItemResponse> itemResponses;
    private long totalNumberItems;
    public InvoiceResponse(Invoice invoice, List<ItemResponse> listitems) {
        super();
       this.invoiceId =invoice.getInvoiceId();
       this.total = invoice.getTotal();
       this.date = invoice.getDate();
       this.customerId =invoice.getCustomerId();
       this.note = invoice.getNote();
       this.setItemResponses(listitems);
    }
    public InvoiceResponse(InvoiceAggResult invoiceAggResult) {
        this.invoiceId =invoiceAggResult.getInvoiceId();
        this.total = invoiceAggResult.getTotal();
        this.date = invoiceAggResult.getDate();
        this.customerId =invoiceAggResult.getCustomerId();
        this.note = invoiceAggResult.getNote();
        this.setItemResponses(invoiceAggResult.getItem().stream().map(ItemResponse::new).collect(Collectors.toList()));
        this.setTotalNumberItems(invoiceAggResult.getTotalItems());
    }
}

package com.backend.domain.response;

import com.backend.domain.document.Item;
import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.UUID;

@Data
public class ItemResponse {
    private String itemId;
    private String name;
    private long price;
    private String note;
    public ItemResponse(Item item){
        this.setItemId(item.getItemId());
        this.setName(item.getName());
        this.setPrice(item.getPrice());
        this.setNote(item.getNote());
    }
}

package com.backend.domain.response;

import lombok.Data;

@Data
public class BaseResponse {
    private long systemTime = System.currentTimeMillis();
}

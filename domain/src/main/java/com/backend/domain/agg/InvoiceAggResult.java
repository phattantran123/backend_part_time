package com.backend.domain.agg;

import com.backend.domain.document.Invoice;
import com.backend.domain.document.Item;
import lombok.Data;

import java.util.List;

@Data
public class InvoiceAggResult extends Invoice {

    private List<Item> item;
    private long totalItems;

}

package com.backend.report;


import ch.qos.logback.classic.LoggerContext;
import com.beowulf.logging.model.LogMessageBuilder;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@Slf4j
@ComponentScan(value = {"com.backend.*"})
public class InvoiceReportApplication {

    public static void main(String[] args) {
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();

        loggerContext.putProperty("SERVER_GROUP", "tomcat");
        SpringApplication app = new SpringApplication(InvoiceReportApplication.class);
        Map<String, Object> configs = new HashMap<>();
        configs.put("server.servlet.context-path", "/invoice");
        configs.put("server.port", "8082");
        configs.put("security.ignored", "/**");
        app.setDefaultProperties(configs);
        app.run(args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(final ApplicationContext context) {
        return args -> {
            String[] beanNames = context.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            for (String beanName : beanNames) {
                log.info("{} {}", LogMessageBuilder.getInstance()
                        .withAction("BeanCreated")
                        .withKeyAndValue("beanName", beanName)
                        .build());
            }
        };
    }
}

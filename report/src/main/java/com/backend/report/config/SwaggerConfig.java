/*
 * Copyright (C) Tutorica LLC. - All Rights Reserved 2020.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by trongcauta <trongcauhcmus@gmail.com>
 */

package com.backend.report.config;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.AlternateTypeRule;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.http.HttpSession;
import java.util.Collections;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {
    @Bean
    public Docket swaggerDocs(TypeResolver typeResolver) {
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage("com.backend.report.controller"))
                .build();
    }

    private AlternateTypeRule[] alternateTypeRules(TypeResolver typeResolver) {
//        AlternateTypeRule alternateTypeRule1 = new AlternateTypeRule(typeResolver.resolve(CurrentMemberConnected.class), typeResolver.resolve(String.class));
        return new AlternateTypeRule[]{};
    }

    private ApiInfo swaggerApiInfo() {
        return new ApiInfoBuilder().title("Backend Part time Task")
                .contact(new Contact("phat tran", "null", "phattantran123@gmail.com"))
                .description("Backend Part time Task Reporting")
                .version("1.0.0")
                .license("Apache License Version 2.0")
                .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
                .build();
    }
}

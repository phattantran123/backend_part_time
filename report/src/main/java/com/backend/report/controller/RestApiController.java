package com.backend.report.controller;

import com.backend.domain.request.CreateInvoiceRequest;
import com.backend.domain.request.GetInvoiceRequest;
import com.backend.domain.response.GeneralSubmitResponse;
import com.backend.domain.response.InvoiceResponse;
import com.backend.service.inf.InvoiceService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Api(tags = "api_backend")
@RequestMapping(value = "/api")
public class RestApiController {

    @Autowired
    InvoiceService invoiceService;

    @PostMapping("/create")
    public InvoiceResponse createNewInvoice(HttpServletRequest servletRequest,
                                           @RequestBody CreateInvoiceRequest request) {
        return invoiceService.createInvoice(request);
    }

    @PostMapping("/create/random")
    public GeneralSubmitResponse createNewInvoiceRandom(HttpServletRequest servletRequest,
                                                 @RequestParam long value) {
        return invoiceService.createRandomInvoiceWithValue(value);
    }

    @PostMapping("/filter")
    public List<InvoiceResponse> getInvoiceByFilter(HttpServletRequest servletRequest,
                                                  @RequestBody
                                   GetInvoiceRequest request) {
        return invoiceService.getInvoiceByFilter(request);
    }



}

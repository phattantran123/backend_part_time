package com.backend.service.impl;

import com.backend.domain.agg.InvoiceAggResult;
import com.backend.domain.document.Invoice;
import com.backend.domain.document.Item;
import com.backend.domain.repository.inf.InvoiceRepository;
import com.backend.domain.repository.inf.ItemRepository;
import com.backend.domain.request.CreateInvoiceRequest;
import com.backend.domain.request.GetInvoiceRequest;
import com.backend.domain.request.ItemRequest;
import com.backend.domain.response.GeneralSubmitResponse;
import com.backend.domain.response.InvoiceResponse;
import com.backend.domain.response.ItemResponse;
import com.backend.service.inf.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class InvoiceServiceImpl implements InvoiceService {

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    ItemRepository itemRepository;

    @Override
    public InvoiceResponse createInvoice(CreateInvoiceRequest createInvoiceRequestList) {

        List<Item> itemList =new ArrayList<>();
        Invoice invoice = new Invoice();
        long total = 0;
        for (ItemRequest request: createInvoiceRequestList.getItemRequestList()
             ) {
            total+=request.getPrice();

            Item item = new Item(request);
            item.setInvoiceId(invoice.getInvoiceId());
            itemList.add(item);
            itemRepository.save(item);
        }
        invoice.setTotal(total);
        invoice.setCustomerId(createInvoiceRequestList.getCustomerId());
        invoice.setNote(createInvoiceRequestList.getNote());
        invoiceRepository.save(invoice);
        return new InvoiceResponse(invoice,itemList.stream().map(ItemResponse::new).collect(Collectors.toList())) ;
    }

    @Override
    public GeneralSubmitResponse createRandomInvoiceWithValue(long value) {
        try {
            for (int i=0;i <value;i++) {
                Invoice invoice = new Invoice();
                long total = 0;

                int number_items_random = (int) ((Math.random() * (50 - 3)) + 3);
                for (int j=0;j <number_items_random ;j++)
                {
                    Item item = new Item(invoice.getInvoiceId(),j);
                    total+=item.getPrice();
                    itemRepository.save(item);
                }
                invoice.setTotal(total);
                invoice.setCustomerId("random_customer_id");
                invoice.setNote("");
                invoiceRepository.save(invoice);
            }
            return new GeneralSubmitResponse(true);
        }
        catch (Exception e){
            throw e;
        }


    }

    @Override
    public List<InvoiceResponse> getInvoiceByFilter(GetInvoiceRequest request) {
        List<InvoiceAggResult> invoiceAggResults = invoiceRepository.getListInvoice(request);

        return invoiceAggResults.stream().map(InvoiceResponse::new).collect(Collectors.toList());
    }
}

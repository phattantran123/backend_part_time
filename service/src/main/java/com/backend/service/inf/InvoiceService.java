package com.backend.service.inf;

import com.backend.domain.request.CreateInvoiceRequest;
import com.backend.domain.request.GetInvoiceRequest;
import com.backend.domain.response.GeneralSubmitResponse;
import com.backend.domain.response.InvoiceResponse;

import java.util.List;

public interface InvoiceService {
    public InvoiceResponse createInvoice(CreateInvoiceRequest createInvoiceRequestList);
    public GeneralSubmitResponse createRandomInvoiceWithValue(long value);
    public List<InvoiceResponse> getInvoiceByFilter(GetInvoiceRequest request);
}
